package pdm.uninsubria.notescategories;

import android.content.ContentValues;

public class Category {
    private long _id;
    private String name;

    public Category() {
        super();
    }

    public Category(String name) {
        super();
        this.name = name;
    }

    public Category(long id, String name) {
        super();
        this._id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * @return the content values representing this category.
     */
    public ContentValues getAsContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.Categories.COLUMN_NAME, this.name);
        return cv;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
