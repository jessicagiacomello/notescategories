package pdm.uninsubria.notescategories;

import android.provider.BaseColumns;

public class DBContract {
    static final int DATABASE_VERSION = 7;
    static final String DATABASE_NAME = "notescategories.db";

    private DBContract() {}

    static abstract class Notes implements BaseColumns {
        static final String TABLE_NAME = "notes";
        static final String COLUMN_TITLE = "title";
        static final String COLUMN_CREATION_DATE = "creation_date";
        static final String COLUMN_NOTE = "note";
        static final String COLUMN_CATEGORY = "category";
        static final String COLUMN_ATTACHMENT = "attachment";
    }

    static abstract class Categories implements BaseColumns {
        static final String TABLE_NAME = "categories";
        static final String COLUMN_NAME = "name";
    }
}