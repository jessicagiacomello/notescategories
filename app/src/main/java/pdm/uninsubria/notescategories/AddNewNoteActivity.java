package pdm.uninsubria.notescategories;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

public class AddNewNoteActivity extends AppCompatActivity {
    private DBAdapter dbAdapter;
    private ArrayList<Category> categories = new ArrayList<Category>();
    private ArrayAdapter<Category> categoryAdapter;
    private Note currentNote = null;
    private ArrayList<String> categoryNames = new ArrayList<String>();
    public static final int GET_FROM_GALLERY = 1;
    private String attachment = null;
    private int provenienza = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_note);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dbAdapter = DBAdapter.getInstance(this);

        //dropdown menu for categories
        Spinner spinner = findViewById(R.id.dropdownCategories);
        categoryAdapter = new ArrayAdapter<Category>(this, android.R.layout.simple_spinner_dropdown_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(categoryAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        final EditText title = findViewById(R.id.title);
        final EditText note = findViewById(R.id.note);

        Intent resultIntent = new Intent();
        resultIntent.putExtra("NOTE_TITLE", title.getText().toString());
        resultIntent.putExtra("NOTE_NOTE", note.getText().toString());

        Spinner mySpinner = findViewById(R.id.dropdownCategories);
        Long cat = dbAdapter.getCategoryIdByName(mySpinner.getSelectedItem().toString());

        resultIntent.putExtra("NOTE_CAT", cat);

        resultIntent.putExtra("NOTE_ATTACHMENT",attachment);

        if (currentNote != null) {
            if(this.isChanged(title.getText().toString(), note.getText().toString(), cat)) {
                resultIntent.putExtra("ID",currentNote.get_id());
                setResult(RESULT_OK, resultIntent);
            } else {
                setResult(RESULT_CANCELED, resultIntent);
            }
        } else {
            setResult(RESULT_OK, resultIntent);
        }
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("addNewNoteActivity",e.getMessage());
        }
        fillCategories();
        Bundle b = getIntent().getExtras();
        if(b != null) {
            long idNote = b.getLong("_id");
            currentNote = dbAdapter.getNoteById(idNote);
        }
        if (currentNote != null && provenienza==0) {
            EditText title = findViewById(R.id.title);
            EditText note = findViewById(R.id.note);

            title.setText(currentNote.getTitle());
            note.setText(currentNote.getNote());

            Spinner spinner = findViewById(R.id.dropdownCategories);

            Category selectedCategory = dbAdapter.getCategoryById(currentNote.getCategory());

            attachment = currentNote.getAttachment();

            if(attachment!=null){
                File imgFile = new File(attachment);

                if(imgFile.exists()){
                    Bitmap bitmap = null;
                    try {
                        bitmap = BitmapFactory.decodeStream(new FileInputStream(imgFile));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    ImageView img=findViewById(R.id.imageView);
                    img.setImageBitmap(bitmap);
                }
            }

            if (selectedCategory != null) {
                String selectedCategoryName = selectedCategory.getName();
                spinner.setSelection(categoryNames.indexOf(selectedCategoryName));
            } else {
                spinner.setSelection(0);
            }
        }
    }

    private void fillCategories() {
        Cursor cursor = dbAdapter.getAllCategories();
        this.categories.clear();
        cursor.moveToFirst();
        int index = 0;
        Category defaultCategory = new Category(0,getString(R.string.def_category));
        this.categories.add(index, defaultCategory);
        this.categoryNames.add(index, defaultCategory.toString());
        while (!cursor.isAfterLast()) {
            index++;
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.Categories._ID));
            String name = cursor.getString(cursor.getColumnIndex(DBContract.Categories.COLUMN_NAME));

            Category newCategory = new Category(id, name);
            this.categories.add(index, newCategory);
            this.categoryNames.add(index, newCategory.toString());

            cursor.moveToNext();
        }
        cursor.close();
        categoryAdapter.notifyDataSetChanged();
    }

    private boolean isChanged(String title, String note, Long cat) {
        if (!title.equals(currentNote.getTitle()) || !note.equals(currentNote.getNote()) || cat != currentNote.getCategory() || !attachment.equals(currentNote.getAttachment())) {
            return true;
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void onClickUpload(View view) {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GET_FROM_GALLERY);
            } else {
                startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case GET_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
                } else {
                    Toast.makeText(this, R.string.upload_not_allowed, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Detects request codes
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK && data != null) {
            provenienza = 1;
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ImageView imageView1 = findViewById(R.id.imageView);
                imageView1.setImageBitmap(bitmap);
                attachment = saveImage(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("addNewNoteActivity",e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("addNewNoteActivity",e.getMessage());
            }

        }
    }

    public String saveImage(Bitmap bitmap) {
        Random rand = new Random();
        int value = rand.nextInt(10000);
        String fileName = value+".jpg";

        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("uploads", Context.MODE_PRIVATE);
        if (!directory.exists()) {
            directory.mkdir();
        }
        File mypath = new File(directory, fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            Log.e("SAVE_IMAGE", e.getMessage(), e);
            fileName = null;
        }
        return directory.getAbsolutePath()+"/"+fileName;
    }

}
