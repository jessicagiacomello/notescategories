package pdm.uninsubria.notescategories;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.sql.SQLException;

public class DBAdapter {

    private static String TAG = "DBAdapter";
    private static DBAdapter sInstance; //singleton instance

    private SQLiteDatabase db; // reference to the DB
    private DatabaseHelper dbHelper; // reference to the OpenHelper

    public static synchronized DBAdapter getInstance(Context context) {
        if (sInstance == null)
            sInstance = new DBAdapter(context.getApplicationContext());
        return sInstance;
    }

    private DBAdapter(Context context) {
        this.dbHelper = DatabaseHelper.getInstance(context);
    }

    public DBAdapter open() throws SQLException {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());
            throw e;
        }
        return this;
    }

    public void close() {
        db.close();
    }


    /* ========== METODI PER OPERARE SU TABELLA NOTES ========== */
    /**
     * Insert the specified note in the DB.
     * @param note
     * @return rowId or -1 if failed
     */
    public long insertNote(Note note) {
        Log.v(TAG, "Adding note to the database.");
        long idx = db.insert(DBContract.Notes.TABLE_NAME, null, note.getAsContentValue());
        Log.v(TAG, "Added value idx: " + idx);
        return idx;
    }

    /**
     * Delete the Note with the the given idx.
     *
     * @param idx the idx of the note to delete.
     * @return true if deleted, false otherwise.
     */
    public boolean deleteNote(long idx) {
        Log.v(TAG, "Removing Note with idx: " + idx);
        return db.delete(DBContract.Notes.TABLE_NAME,DBContract.Notes._ID + "=" + idx, null) == 1;
    }

    /**
     * Delete the specified Note from the DBx.
     *
     * @param note the note to delete.
     * @return true if deleted, false otherwise.
     */
    public boolean deleteN(Note note) {
        Log.v(TAG, "Removing Note with idx: " + note.get_id());
        return db.delete(DBContract.Notes.TABLE_NAME,
                DBContract.Notes._ID + "=" + note.get_id(),
                null) == 1;
    }

    /**
     * Return a Cursor over the list of all notes in the database
     *
     * @return Cursor over all notes
     */
    public Cursor getAllNotes() {
        return db.query(DBContract.Notes.TABLE_NAME, null, null, null, null, null, DBContract.Notes.COLUMN_CREATION_DATE);
    }

    /**
     * Return a Cursor over the list of all notes in the database filtered by the given category
     *
     * @return Cursor over filtered notes
     */
    public Cursor getNotesByCategory(long cat) {
        if (cat == -1) {
            return getAllNotes();
        } else {
            String[] args = new String[] {Long.toString(cat)};
            return db.query(DBContract.Notes.TABLE_NAME, null, DBContract.Notes.COLUMN_CATEGORY+" = ?", args, null, null, DBContract.Notes.COLUMN_CREATION_DATE);
        }
    }

    /**
     * Return a Note by its id
     *
     * @return Note
     */
    public Note getNoteById(long noteId) {
        String[] args = new String[] {Long.toString(noteId)};
        Cursor cursor = db.query(DBContract.Notes.TABLE_NAME, null, "_id = ?", args, null, null, null);
        Note noteObj = null;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.Notes._ID));
            String title = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_TITLE));
            String note = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_NOTE));
            long category = cursor.getLong(cursor.getColumnIndex(DBContract.Notes.COLUMN_CATEGORY));
            long created = cursor.getLong(cursor.getColumnIndex(DBContract.Notes.COLUMN_CREATION_DATE));
            String attachment = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_ATTACHMENT));
            // add the element to the array list
            noteObj = new Note(id, title, note, category, created, attachment);
            // move to the next element
            cursor.moveToNext();
        }
        cursor.close();
        return noteObj;
    }

    /**
     * Update an existing Note
     *
     * @return boolean
     */
    public boolean updateNote(Note note) {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.Notes.COLUMN_TITLE,note.getTitle());
        cv.put(DBContract.Notes.COLUMN_NOTE,note.getNote());
        cv.put(DBContract.Notes.COLUMN_CATEGORY,note.getCategory());
        cv.put(DBContract.Notes.COLUMN_CREATION_DATE,note.getCreation_date().getTimeInMillis());
        cv.put(DBContract.Notes.COLUMN_ATTACHMENT,note.getAttachment());
        String[] whereArgs = new String[] {Long.toString(note.get_id())};
        int res = db.update(DBContract.Notes.TABLE_NAME,cv,"_id = ?", whereArgs);
        if (res == 1) {
            return true;
        } else {
            return false;
        }
    }

    /* ========== METODI PER OPERARE SU TABELLA CATEGORIES ========== */
    /**
     * Insert the specified category in the DB.
     * @param category
     * @return rowId or -1 if failed
     */
    public long insertCategory(Category category) {
        Log.v(TAG, "Adding category to the database.");
        long idx = db.insert(DBContract.Categories.TABLE_NAME, null, category.getAsContentValue());
        Log.v(TAG, "Added value idx: " + idx);
        return idx;
    }

    /**
     * Delete the Category with the the given idx.
     *
     * @param idx the idx of the category to delete.
     * @return true if deleted, false otherwise.
     */
    public boolean deleteCategory(long idx) {
        Log.v(TAG, "Removing Category with idx: " + idx);
        return db.delete(DBContract.Categories.TABLE_NAME,DBContract.Categories._ID + "=" + idx, null) == 1;
    }

    /**
     * Delete the specified Category from the DBx.
     *
     * @param category the category to delete.
     * @return true if deleted, false otherwise.
     */
    public boolean deleteC(Category category) {
        Log.v(TAG, "Removing Category with idx: " + category.get_id());
        if(db.delete(DBContract.Categories.TABLE_NAME, DBContract.Categories._ID + "=" + category.get_id(), null) == 1){
            ContentValues cv = new ContentValues();
            cv.put(DBContract.Notes.COLUMN_CATEGORY,0);
            String[] whereArgs = new String[] {Long.toString(category.get_id())};
            db.update(DBContract.Notes.TABLE_NAME, cv, DBContract.Notes.COLUMN_CATEGORY+" = ?",whereArgs);
            return true;
        }
        return false;
    }

    /**
     * Return a Cursor over the list of all categories in the database
     *
     * @return Cursor over all categories
     */
    public Cursor getAllCategories() {
        return db.query(DBContract.Categories.TABLE_NAME, null, null, null, null, null, null);
    }

    /**
     * Return category id by its name
     *
     * @return Long category id
     */
    public long getCategoryIdByName(String name) {
        String[] tableColumns = new String[] {DBContract.Categories._ID};
        String whereClause = DBContract.Categories.COLUMN_NAME + "= ?";
        String[] whereArgs = new String[] {name};
        Cursor cursor = db.query(DBContract.Categories.TABLE_NAME,tableColumns,whereClause,whereArgs,null,null,null);
        long cat = 0;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            cat = cursor.getLong(cursor.getColumnIndex(DBContract.Categories._ID));
            cursor.moveToNext();
        }
        cursor.close();
        return cat;
    }

    /**
     * Return a Category by its id
     *
     * @return Category
     */
    public Category getCategoryById(long noteId) {
        String[] args = new String[] {Long.toString(noteId)};
        Cursor cursor = db.query(DBContract.Categories.TABLE_NAME, null, "_id = ?", args, null, null, null);
        Category categoryObj = null;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.Categories._ID));
            String name = cursor.getString(cursor.getColumnIndex(DBContract.Categories.COLUMN_NAME));

            categoryObj = new Category(id, name);

            cursor.moveToNext();
        }
        cursor.close();
        return categoryObj;
    }
}
