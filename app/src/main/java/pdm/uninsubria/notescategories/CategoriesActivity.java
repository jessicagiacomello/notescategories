package pdm.uninsubria.notescategories;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

public class CategoriesActivity extends AppCompatActivity {
    private DBAdapter dbAdapter;
    private ArrayList<Category> categoriesList;
    private ArrayAdapter<Category> listViewAdapter;
    private static final String TAG = "CategoriesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dbAdapter = DBAdapter.getInstance(this);

        final ListView listView = findViewById(R.id.listViewCategories);

        categoriesList = new ArrayList<Category>();

        listViewAdapter = new ArrayAdapter<Category>(this, android.R.layout.simple_list_item_1, categoriesList);
        listView.setAdapter(listViewAdapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                onLongClick(pos);
                return true;
            }
        });

    }

    public void onClickAddCategory(View view) {
        final EditText editText = findViewById(R.id.editNewCategory);
        String categoryName = editText.getText().toString();
        if (categoryName.length()==0) {
            Toast.makeText(getApplicationContext(), getString(R.string.empty_category), Toast.LENGTH_LONG).show();
            return;
        }
        Category cat = new Category(categoryName);

        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("main_activity",e.getMessage());
        }

        long idx = dbAdapter.insertCategory(cat);
        cat.set_id(idx);

        categoriesList.add(0, cat);
        listViewAdapter.notifyDataSetChanged();

        editText.setText("");

        editText.clearFocus();
    }

    public void onLongClick(final int position) {
        final ListView listView = findViewById(R.id.listViewCategories);
        final Category item = (Category) listView.getItemAtPosition(position);

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.alert_delete_category));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.delete),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dbAdapter.deleteC(item);
                        categoriesList.remove(position);
                        listViewAdapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), R.string.category_deleted, Toast.LENGTH_LONG).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
        }
        fillData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
        dbAdapter.close();
    }

    private void fillData() {
        Cursor cursor = dbAdapter.getAllCategories();
        this.categoriesList.clear();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.Categories._ID));
            String name = cursor.getString(cursor.getColumnIndex(DBContract.Categories.COLUMN_NAME));

            this.categoriesList.add(0, new Category(id, name));

            cursor.moveToNext();
        }
        cursor.close();
        listViewAdapter.notifyDataSetChanged();
    }
}
