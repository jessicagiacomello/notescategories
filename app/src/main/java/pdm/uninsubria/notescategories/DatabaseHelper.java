package pdm.uninsubria.notescategories;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper sInstance;

    private static final String LOG_TAG = "DatabaseHelper";

    private static final String SQL_CREATE_TABLE_NOTES = "CREATE TABLE "+
            DBContract.Notes.TABLE_NAME+" ( " +
            DBContract.Notes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DBContract.Notes.COLUMN_TITLE + " TEXT not null, "+
            DBContract.Notes.COLUMN_NOTE + " TEXT not null, "+
            DBContract.Notes.COLUMN_CATEGORY + " INTEGER DEFAULT 0, "+
            DBContract.Notes.COLUMN_CREATION_DATE + " LONG, "+
            DBContract.Notes.COLUMN_ATTACHMENT + " TEXT);";

    private static final String SQL_SELECT_ALL_NOTES = "SELECT * FROM "+DBContract.Notes.TABLE_NAME;

    private static final String SQL_DELETE_TABLE_NOTES = "DROP TABLE IF EXISTS "+DBContract.Notes.TABLE_NAME;

    private static final String SQL_CREATE_TABLE_CATEGORIES = "CREATE TABLE "+
            DBContract.Categories.TABLE_NAME+" ( " +
            DBContract.Categories._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DBContract.Categories.COLUMN_NAME + " TEXT UNIQUE not null);";

    private static final String SQL_SELECT_ALL_CATEGORIES = "SELECT * FROM "+DBContract.Categories.TABLE_NAME;

    private static final String SQL_DELETE_TABLE_CATEGORIES = "DROP TABLE IF EXISTS "+DBContract.Categories.TABLE_NAME;

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private DatabaseHelper(Context context) {
        super(context, DBContract.DATABASE_NAME, null, DBContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v(LOG_TAG, "Creating database: " + DBContract.DATABASE_NAME);
        db.execSQL(SQL_CREATE_TABLE_NOTES);
        db.execSQL(SQL_CREATE_TABLE_CATEGORIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE_NOTES);
        db.execSQL(SQL_DELETE_TABLE_CATEGORIES);
        this.onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.onUpgrade(db, oldVersion, newVersion);
    }
}
