package pdm.uninsubria.notescategories;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {
    private DBAdapter dbAdapter;
    private ArrayList<Note> notesList;
    private static ArrayAdapter<Note> listViewAdapter;
    static final int NEW_NOTE_REQUEST = 1;
    static final int UPDATE_NOTE_REQUEST = 2;
    private static final String TAG = "MainActivity";
    private ArrayList<Category> categories = new ArrayList<Category>();
    private ArrayAdapter<Category> categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        dbAdapter = DBAdapter.getInstance(this);

        final ListView listView = findViewById(R.id.listViewNotes);

        notesList = new ArrayList<Note>();

        listViewAdapter = new ArrayAdapter<Note>(this, android.R.layout.simple_list_item_1, notesList);
        listView.setAdapter(listViewAdapter);

        //dropdown menu for categories
        final Spinner spinner = findViewById(R.id.dropdownCategories);
        categoryAdapter = new ArrayAdapter<Category>(this, android.R.layout.simple_spinner_dropdown_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(categoryAdapter);

        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        if(spinner.getSelectedItem().toString().equals(getString(R.string.all))){
                            filterList(-1l);
                        } else {
                            Long cat = dbAdapter.getCategoryIdByName(spinner.getSelectedItem().toString());
                            filterList(cat);
                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        //empty
                    }
                });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                onLongClick(pos);
                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                onClick(pos);
            }
        });

    }

    private void filterList(Long cat) {
        Cursor cursor = dbAdapter.getNotesByCategory(cat);
        this.notesList.clear();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.Notes._ID));
            String title = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_TITLE));
            String note = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_NOTE));
            long category = cursor.getLong(cursor.getColumnIndex(DBContract.Notes.COLUMN_CATEGORY));
            long created = cursor.getLong(cursor.getColumnIndex(DBContract.Notes.COLUMN_CREATION_DATE));
            String attachment = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_ATTACHMENT));

            this.notesList.add(0, new Note(id, title, note, category, created, attachment));

            cursor.moveToNext();
        }
        cursor.close();
        listViewAdapter.notifyDataSetChanged();
    }

    private boolean onClick(int pos) {
        final ListView listView = findViewById(R.id.listViewNotes);
        Note n = (Note) listView.getItemAtPosition(pos);

        Intent i = new Intent(getApplicationContext(), AddNewNoteActivity.class);
        Bundle b = new Bundle();
        b.putLong("_id", n.get_id());
        i.putExtras(b);
        startActivityForResult(i, UPDATE_NOTE_REQUEST);
        return true;
    }

    public void onLongClick(final int position) {
        final ListView listView = findViewById(R.id.listViewNotes);
        final Note item = (Note) listView.getItemAtPosition(position);

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.alert_delete_note));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.delete),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dbAdapter.deleteN(item);
                        notesList.remove(position);
                        listViewAdapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), R.string.note_deleted, Toast.LENGTH_LONG).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_categories) {
            Intent i = new Intent(getApplicationContext(), CategoriesActivity.class);
            startActivity(i);
            return true;
        } else if (id == R.id.action_new_note) {
            Intent i = new Intent(getApplicationContext(), AddNewNoteActivity.class);
            startActivityForResult(i, NEW_NOTE_REQUEST);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_NOTE_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String title = data.getStringExtra("NOTE_TITLE");
                String note = data.getStringExtra("NOTE_NOTE");
                long cat = data.getLongExtra("NOTE_CAT",0);
                String attachment = data.getStringExtra("NOTE_ATTACHMENT");
                if (!title.equals("") || !note.equals("") || attachment != null) { //at least one of the three fields must be filled in
                    addNewNote(title, note, cat, attachment);
                }
                return;
            }
        } else if (requestCode == UPDATE_NOTE_REQUEST) {
            if (resultCode == RESULT_OK) {
                Long id = data.getLongExtra("ID",0);
                String title = data.getStringExtra("NOTE_TITLE");
                String note = data.getStringExtra("NOTE_NOTE");
                long cat = data.getLongExtra("NOTE_CAT", 0);
                String attachment = data.getStringExtra("NOTE_ATTACHMENT");
                if (!title.equals("") || !note.equals("") || attachment != null) { //at least one of the three fields must be filled in
                    updateNote(id, title, note, cat, attachment);
                } else { //delete because it is empty
                    Note n = dbAdapter.getNoteById(id);
                    dbAdapter.deleteN(n);
                }
                return;
            } else if (resultCode == RESULT_CANCELED) {
                return;
            }
        }
    }

    private void updateNote(long id, String title, String note, long cat, String attachment) {
        Note updatedNote = new Note(id,title,note,cat,new GregorianCalendar().getTimeInMillis(), attachment);

        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("main_activity",e.getMessage());
        }

        if(dbAdapter.updateNote(updatedNote)){
            fillData();
        } else {
            Toast.makeText(this, R.string.update_error, Toast.LENGTH_LONG).show();
        }

    }

    public void addNewNote(String title, String note, long cat, String attachment) {

        Note newNote = new Note(title, note, cat, attachment);

        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("main_activity",e.getMessage());
        }

        long idx = dbAdapter.insertNote(newNote);
        newNote.set_id(idx);

        notesList.add(0, newNote);
        listViewAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
        }
        fillData();
        fillCategories();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
        dbAdapter.close();
    }

    private void fillData() {
        Cursor cursor = dbAdapter.getAllNotes();
        this.notesList.clear();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.Notes._ID));
            String title = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_TITLE));
            String note = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_NOTE));
            long category = cursor.getLong(cursor.getColumnIndex(DBContract.Notes.COLUMN_CATEGORY));
            long created = cursor.getLong(cursor.getColumnIndex(DBContract.Notes.COLUMN_CREATION_DATE));
            String attachment = cursor.getString(cursor.getColumnIndex(DBContract.Notes.COLUMN_ATTACHMENT));

            this.notesList.add(0, new Note(id, title, note, category, created, attachment));

            cursor.moveToNext();
        }
        cursor.close();
        listViewAdapter.notifyDataSetChanged();
    }

    private void fillCategories() {
        Cursor cursor = dbAdapter.getAllCategories();
        this.categories.clear();
        cursor.moveToFirst();
        int index = 0;
        Category allCategory = new Category(-1,getString(R.string.all));
        Category defaultCategory = new Category(0,getString(R.string.def_category));
        this.categories.add(index, allCategory);
        index++;
        this.categories.add(index, defaultCategory);
        while (!cursor.isAfterLast()) {
            index++;
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.Categories._ID));
            String name = cursor.getString(cursor.getColumnIndex(DBContract.Categories.COLUMN_NAME));

            Category newCategory = new Category(id, name);
            this.categories.add(index, newCategory);

            cursor.moveToNext();
        }
        cursor.close();
        categoryAdapter.notifyDataSetChanged();
        Spinner spinner = findViewById(R.id.dropdownCategories);
        spinner.setSelection(0);
    }
}
