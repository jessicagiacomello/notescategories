package pdm.uninsubria.notescategories;

import android.content.ContentValues;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Note {
    private long _id;
    private String title;
    private String note;
    private GregorianCalendar creation_date;
    private long category = 0;
    private String attachment;

    public Note() {
        super();
    }

    public Note(String title, String note, long category, String attachment) {
        super();
        this.title = title;
        this.note = note;
        this.creation_date = new GregorianCalendar();
        this.category = category;
        this.attachment = attachment;
    }

    public Note(long id, String title, String note, long category, long created, String attachment) {
        super();
        this._id = id;
        this.title = title;
        this.note = note;
        this.category = category;

        GregorianCalendar createOn = new GregorianCalendar();
        createOn.setTimeInMillis(created);
        this.creation_date = createOn;

        this.attachment = attachment;
    }

    @Override
    public String toString() {
        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).format(creation_date.getTime());
        return currentDate + "\n" + title;
    }

    /**
     * @return the content values representing this note.
     */
    public ContentValues getAsContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.Notes.COLUMN_TITLE, this.title);
        cv.put(DBContract.Notes.COLUMN_NOTE, this.note);
        cv.put(DBContract.Notes.COLUMN_CATEGORY, this.category);
        cv.put(DBContract.Notes.COLUMN_CREATION_DATE, this.creation_date.getTimeInMillis());
        cv.put(DBContract.Notes.COLUMN_ATTACHMENT, this.attachment);
        return cv;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public GregorianCalendar getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(GregorianCalendar creation_date) {
        this.creation_date = creation_date;
    }

    public long getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
}
